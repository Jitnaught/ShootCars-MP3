#include <deque>

#include "stdafx.h"
#include "inc/main.h"

#include "script.h"

#define PI 3.14159265358979323846f

std::deque<Vehicle> vehicles;

float DegreesToRadians(float degree)
{
	return degree * (PI / 180.0f);
}

Quaternion Vec3RotToQuat(Vector3 axis, float angle)
{
	Vector3 normalized = axis.Normalize();

	float half = angle * 0.5f;
	float sin = sinf(half);
	float cos = cosf(half);

	Quaternion result;
	result.x = normalized.x * sin;
	result.y = normalized.y * sin;
	result.z = normalized.z * sin;
	result.w = cos;

	return result;
}

Ped GetPlayerPed()
{
	if (PLAYER::DOES_MAIN_PLAYER_EXIST())
	{
		return PLAYER::GET_PLAYER_PED(PLAYER::GET_PLAYER_ID());
	}

	return NULL;
}

void DeleteVeh(Vehicle veh)
{
	if (veh != NULL && VEHICLE::DOES_VEHICLE_EXIST(veh))
	{
		Hash model = VEHICLE::GET_VEHICLE_MODEL(veh);

		VEHICLE::DELETE_VEHICLE(&veh);
		STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(model);
	}
}

void ClearVehicles()
{
	if (vehicles.size() > 0)
	{
		for (size_t i = 0; i < vehicles.size(); i++)
		{
			DeleteVeh(vehicles[i]);
		}

		vehicles.clear();
	}
}

void main()
{
	bool enabled = GetPrivateProfileInt(L"SETTINGS", L"ENABLED_BY_DEFAULT", 0, L".\\ShootCars.ini") != 0;
	int toggleKey = GetPrivateProfileInt(L"SETTINGS", L"TOGGLE_KEY", VK_F8, L".\\ShootCars.ini");

	int lastSpawnVeh = 0;

	while (true)
	{
		if (IsKeyJustUp(toggleKey))
		{
			enabled = !enabled;

			const char *printStr;
			if (enabled) printStr = "ShootCars enabled";
			else printStr = "ShootCars disabled";

			HUD::PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", printStr, 1000, 1);

			if (!enabled)
			{
				ClearVehicles();
			}
		}

		if (enabled)
		{
			Ped plrPed = GetPlayerPed();

			if (plrPed != NULL && PED::DOES_PED_EXIST(plrPed))
			{
				if (PED::IS_PED_DEAD(plrPed))
				{
					ClearVehicles();
				}
				else if (PED::IS_PED_SHOOTING(plrPed))
				{
					int gameTime = MISC::GET_GAME_TIMER();

					if (gameTime - lastSpawnVeh >= 500)
					{
						if (vehicles.size() >= 3)
						{
							DeleteVeh(vehicles[0]);

							vehicles.pop_front();
						}

						int rand = std::rand() % (sizeof(vehicleNames) / sizeof(vehicleNames[0]));
						Hash vehHash = MISC::GET_HASH_KEY(vehicleNames[rand].c_str());

						for (int i = 0; i < 100; i++)
						{
							if (STREAMING::HAS_MODEL_LOADED(vehHash)) break;

							STREAMING::REQUEST_MODEL(vehHash);
							scriptWait(1);
						}
						
						if (STREAMING::HAS_MODEL_LOADED(vehHash))
						{
							Vector3 minDimension, maxDimension;
							MISC::GET_MODEL_DIMENSIONS(vehHash, &minDimension, &maxDimension);

							Vector3 spawnPos = PED::GET_OFFSET_FROM_PED_IN_WORLD_COORDS(plrPed, 0.0f, maxDimension.y + 5.0f, 0.5f);
							Vector3 camRot = CAM::GET_GAMEPLAY_CAM_ROT();
							Quaternion camQuat = Vec3RotToQuat(camRot, abs(DegreesToRadians(camRot.z)));
							
							Vehicle veh = VEHICLE::CREATE_VEHICLE(vehHash, spawnPos.x, spawnPos.y, spawnPos.z, 0.0f, true, false);

							if (veh != NULL && VEHICLE::DOES_VEHICLE_EXIST(veh))
							{
								VEHICLE::SET_VEHICLE_QUATERNION(veh, camQuat.x, camQuat.y, camQuat.z, camQuat.w);
								VEHICLE::APPLY_FORCE_TO_VEHICLE(veh, 2, 0.0f, 9999.0f, 0.0f, 0.0f, -300.0f, 0.0f, 0, true, true, true);
								vehicles.push_back(veh);
							}
						}

						lastSpawnVeh = gameTime;
					}
				}
			}
		}

		scriptWait(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
