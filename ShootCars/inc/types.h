#pragma once

#include <Windows.h>

typedef int Entity;
typedef int Ped;
typedef int Vehicle;
typedef int Cam;
typedef int Group;
typedef int Pickup;
typedef int Object;
typedef int Weapon;
typedef int Blip;
typedef int Camera;
typedef int ScrHandle;
typedef int FireId;
typedef int Rope;
typedef int Interior;
typedef unsigned int Player;
typedef unsigned long Hash;
typedef unsigned long Void;
typedef unsigned long Any;

struct Request_s
{
	int index;
	int unk;
};

typedef struct Vector3
{
	float x, y, z;

	Vector3() { }

	Vector3(float X, float Y, float Z)
	{
		x = X;
		y = Y;
		z = Z;
	}

	Vector3 operator+(Vector3 b)
	{
		Vector3 newVec;

		newVec.x = x + b.x;
		newVec.y = y + b.y;
		newVec.z = z + b.z;

		return newVec;
	}

	Vector3 operator*(float multiplier)
	{
		Vector3 newVec;

		newVec.x = x * multiplier;
		newVec.y = y * multiplier;
		newVec.z = z * multiplier;

		return newVec;
	}

	float Length()
	{
		return sqrtf((x * x) + (y * y) + (z * z));
	}

	Vector3 Normalize()
	{
		float length = Length();

		if (length == 0) return Vector3(0, 0, 0);

		float inv = 1.0f / length;
		Vector3 newVec;

		newVec.x = x * inv;
		newVec.y = y * inv;
		newVec.z = z * inv;

		return newVec;
	}

} Vector3;

typedef struct Quaternion
{
	float x, y, z, w;
} Quaternion;